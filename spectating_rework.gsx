// CoD4 Spectating Rework
// ----------------------
// Head to README.md for details.

// License: The MIT License
// Copyright (c) 2016 Rafał Florczak [andr]
// You can find more information in the LICENSE file.

init()
{
    level endon("game_ended");
    level.FREE_SPECT = -1;
    level.TICKS_BEFORE_SWITCH = 4;

    while (true)
    {
        level waittill("connected", player);
        player thread on_player_spectate();
    }
}

on_player_spectate()
{
    self endon("disconnect");

    while (true)
    {
        self set_spectating(level.FREE_SPECT);
        self.previous_target = level.FREE_SPECT;

        self.can_switch_target = true;
        self.ticks_with_same_target = 0;

        while (true)
        {
            if (self.sessionState == "playing")
                break;

            if (self.can_switch_target)
                self handle_spectate_controls();

            self handle_spectate_switch_delay();

            wait .01;
        }

        self waittill("joined_spectators");
    }
}

handle_spectate_controls()
{
    if (self meleeButtonPressed())
        self set_spectating(level.FREE_SPECT);
    else if (self attackButtonPressed() && self.can_switch_target)
    {
        new_target = self get_next_spect_number();
        self set_spectating(new_target);
    }
    else if (self aimButtonPressed() && self.can_switch_target)
    {
        new_target = self get_previous_spect_number();
        self set_spectating(new_target);
    }
}

handle_spectate_switch_delay()
{
    if (self.previous_target != self.spectating)
    {
        self.previous_target = self.spectating;
        self.ticks_with_same_target = 0;
        self.can_switch_target = false;
    }

    if (!self.can_switch_target)
    {
        self.ticks_with_same_target++;

        if (self.ticks_with_same_target >= level.TICKS_BEFORE_SWITCH)
            self.can_switch_target = true;
    }
}

set_spectating(target)
{
    self.spectating = target;
    self.spectatorClient = target;
}

get_next_spect_number()
{
    playing = get_playing_players();
    spectating = self.spectating;

    for (i = 0; i < playing.size; i++)
    {
        if (playing[i] == spectating)
            return playing[(i + 1) % playing.size];
    }

    return get_default_spect_number();
}

get_previous_spect_number()
{
    spectating = self.spectating;

    playing = get_playing_players();
    for (i = 0; i < playing.size; i++)
    {
        if (playing[i] == spectating)
        {
            ret = (i - 1) % playing.size;
            if (ret == -1)
                return playing[playing.size - 1];
            else
                return playing[ret];
        }
    }

    return get_default_spect_number();
}

get_default_spect_number()
{
    playing = get_playing_players();
    if (playing.size > 0)
        return playing[0];

    return -1;
}

get_playing_players()
{
    players = getentarray("player", "classname");
    playing = [];

    for (i = 0; i < players.size; i++)
    {
        if (players[i].sessionstate == "playing")
            playing[playing.size] = players[i] getEntityNumber();
    }

    return playing;
}
